import {
    GoogleMap,
    withGoogleMap,
    withScriptjs,
    Marker,
  } from "react-google-maps";
  import React, { Component } from "react";
  
  /**
   * settings for retrive google map
   */
  const GoogleMapWithOptions = withGoogleMap((props) => (
    <GoogleMap
      defaultZoom={props.zoom}
      defaultCenter={props.center}
      defaultOptions={{
        streetViewControl: false,
        scaleControl: false,
        mapTypeControl: false,
        panControl: false,
        zoomControl: false,
        rotateControl: false,
        fullscreenControl: false,
        disableDefaultUI: true,
        scrollwheel: false,
      }}
    >
      {props.markers.map((marker, index) => {
        return (
          <Marker key={index} position={{ lat: marker.lat, lng: marker.lng }} />
        );
      })}
    </GoogleMap>
  ));
  
  export class Map extends Component {
    render() {
      console.log('props.markers', this.props.markers);
      return (
        <div >
          <GoogleMapWithOptions
            {...this.props}
            containerElement={this.props.containerElement}
            mapElement={this.props.mapElement}
          />
          ,  
        </div>
      );
    }
  }
  export default Map;