import React, { Component } from "react";
import Map from './googleMap'
import Axios from 'axios';
import Geocode from "react-geocode";

Geocode.setApiKey("AIzaSyBJsmoKJ-gpc4ZDzbHa5mrDzvk11QEqMbw");
export class Dashboard extends Component {
    constructor(props) {
        super();
        this.state = {
            allData: [],
            allStates: [],
            stateWiseCities: [],
            latitude: null,
            longitude: null
        }
    }

    componentDidMount() {
        // get all cities api
        Axios.get("https://indian-cities-api-nocbegfhqg.now.sh/cities").then((cities) => {
            this.setState(
                {
                    allData: cities.data
                }, () => this.getUniqueState());
        },
            this.getCoOrdinateByAddress('Bangalore') //initial city to show
        );
    }

    /**
     * Unique states
     */
    getUniqueState = () => {
        const states = [...new Set(this.state.allData.map(item => item.State))];
        this.setState({
            allStates: states
        })
    }
    /**
     * on select particular state 
     */
    selectedStateHandler = (event) => {
        let stateWiseCities = this.state.allData.filter(alldata => alldata.State === event.target.value);
        this.setState({
            stateWiseCities: stateWiseCities
        });
    }
    /**
     * on select city show marked in map
     */
    onSelectCity = (value) => {
        this.getCoOrdinateByAddress(value)
    }
    /**
     * getting coordinates by place name
     */
    getCoOrdinateByAddress = (address) => {
        Geocode.fromAddress(address).then(
            response => {
                const { lat, lng } = response.results[0].geometry.location;
                console.log(lat, lng, address)
                this.setState({
                    latitude: lat,
                    longitude: lng
                })
            },
            error => {
                console.error(error);
            }
        );
    }
    render() {
        return (
            <div>
                <nav class="navbar navbar-expand-sm bg-dark">

                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link" href="#">About Us</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Find Location</a>
                        </li>
                    </ul>

                </nav>
                <div className="dropdown">
                <label className="ml-8">Select state</label>
                <select className="ml-8 select-state" onChange={this.selectedStateHandler}>
                    <option >
                        Select State
                                            </option>
                    {
                        this.state.allStates.map((res, i) => {
                            return (
                                <option key={res} value={res}>{res} </option>
                            )
                        })
                    }
                </select>
                </div>
                <div className="row">
                    <div className="col-md-12 col-lg-6 ">
                    <div class="card">
                        <div className="cities-list-container">
                            {this.state.stateWiseCities.length !== 0 ? 
                            <div className="cities-container">
                                <h4>Please click on city to see city on map</h4>
                                <ul>
                                     {  this.state.stateWiseCities.map((res, i) => {
                                        return (
                                            <li className="eachcity" onClick={() => this.onSelectCity(res.City)} key={i}>{res.City}</li>
                                        )
                                    }) 
                                }
                                </ul>
                            </div> : <p className="light-text">Please select a state to display cities</p> }
                        </div>
                    </div>
                    </div>
                    <div className="col-md-12 col-lg-6">
                        <div class="card">

                                <Map
                                    containerElement={<div style={{ height: `89vh` }} />}
                                    mapElement={<div style={{ height: `110%` }} />}
                                    center={{
                                        lat: 20.5937, lng: 78.9629
                                    }}
                                    zoom={4.7}
                                    markers={[{ lat: this.state.latitude, lng: this.state.longitude }]}
                                >

                                </Map>
                        </div>
                    </div>
                </div>

            </div>
        )
    }
}

export default Dashboard;